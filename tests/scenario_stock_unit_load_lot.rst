============================
Stock Unit Load Lot Scenario
============================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard, Report
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()
    >>> tomorrow = today + relativedelta(days=1)
    >>> time_ = datetime.datetime.now().time()
    >>> time_ = time_.replace(microsecond=0)

Install stock_unit_load_lot::

    >>> config = activate_modules('stock_unit_load_lot')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> production_loc, = Location.find([('type', '=', 'production')])
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])

Create an unit load::

    >>> UnitLoad = Model.get('stock.unit_load')
    >>> unit_load = UnitLoad()
    >>> unit_load.end_date = unit_load.start_date + relativedelta(minutes=5)
    >>> unit_load.production_type = 'location'
    >>> unit_load.production_location = output_loc
    >>> unit_load.product = product
    >>> unit_load.cases_quantity = 5
    >>> unit_load.production_location = production_loc
    >>> unit_load.save()

Add moves with Lot::

    >>> Lot = Model.get('stock.lot')
    >>> lot = Lot(product=unit_load.product, number='L-002')
    >>> lot.save()
    >>> unit_load.quantity = Decimal('35.0')
    >>> move = unit_load.production_moves[0]
    >>> move.to_location = storage_loc
    >>> move.lot = lot
    >>> unit_load.save()
    >>> unit_load.lot == lot
    True

Move and check lot copy::

    >>> unit_load.click('assign')
    >>> unit_load.click('do')
    >>> move_ul = Wizard('stock.unit_load.do_move', [unit_load])
    >>> move_ul.form.planned_date = datetime.datetime.combine(tomorrow, time_)
    >>> move_ul.form.location = output_loc
    >>> move_ul.execute('move_')
    >>> unit_load.reload()
    >>> unit_load.last_moves[0].lot == lot
    True