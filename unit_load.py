# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['UnitLoad']


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    lot = fields.Function(fields.Many2One('stock.lot', 'Lot',
        domain=[('product', '=', Eval('product'))],
        depends=['product']), 'get_lot', searcher='search_lot')

    @classmethod
    def get_lot(cls, records, name):
        res = {}
        for record in records:
            res[record.id] = None
            for move in record.production_moves:
                if move.product == record.product and move.lot:
                    res[record.id] = move.lot.id
                    break
        return res

    @classmethod
    def search_lot(cls, name, clause):
        return [('moves.%s' % clause[0],) + tuple(clause[1:])]
